# StarAlienBOT-versao-atual

`Caso usem os arquivos do meu bot, favor modificar as informações de TODOS os comando caso ter informações minhas nos codes... O intuito é ajudar dando referências e bases para iniciar ou implementar algumas das funções no seus projetos de bots....  além disso sejam criativos, não tenha uma atitude de quem "kiba" code de bots...`


![Legenda](https://i.imgur.com/LLIQKIl.png)

`Outros detalhes do  bot:` https://staralien.glitch.me

`Discord do criador do bot:` https://discord.gg/AUHtzRr

# Política de Uso StarAlien-BOT

Este Termo estabelece as regras para utilização dos recursos aqui presentes . Informamos que através do seu uso você está aceitando as condições apresentadas neste contrato. Se você não concorda com os termos e condições apresentados, não utilize os recursos.

Por favor, note que nós podemos mudar nossa Política de Uso, é sua a responsabilidade manter-se atualizado e aderir às políticas aqui postadas.

# Ações proibidas

1 - Usar os recursos para distribuir, disseminar ou oferecer para fazer o mesmo com relação a qualquer conteúdo de caráter 

2 - Usar os recursos para difamatório, ofensivo, abusivo, fraudulento, infrator, obsceno ou de outra forma censurável ilegal;

3 - Usar os recursos para Distribuir intencionalmente vírus, worms, defeitos, cavalos de Tróia, arquivos corrompidos, trotes, ou quaisquer outros itens de natureza destrutiva ou enganosa;

4 - Usar os recursos para violar os direitos legais (tais como direitos de privacidade e publicidade) de outros;

5 - Usar os recursos para promover ou encorajar atividade ilegal;

6 - Usar os recursos para exibir qualquer conteúdo no Serviço que contenha qualquer conteúdo relacionado com ódio ou violência ou contém quaisquer outros materiais, produtos ou serviços que violem ou encorajem conduta que viole as leis penais, quaisquer outras leis aplicáveis, ou quaisquer direitos de terceiros;

7 - Usar os recursos para se passar pelo meu bot usando como forma de "manchar" a imagem do bot....